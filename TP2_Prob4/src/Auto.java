
public class Auto 
{
	public static final int LONG_CHAINE = 15;
	private String marque, modele, couleur;
	private int prix, puissance, anneeFabrication;
	
	Auto()
	{
		this.marque = "NULL";
		this.modele = "NULL";
		this.couleur = "NULL";
		this.puissance = 0;
		this.prix = 0;
		this.anneeFabrication = 0;
	}
	
	Auto(String marque, String modele, String couleur, int prix, int puissance, int anneeFabrication)
	{
		this.marque = marque;
		this.modele = modele;
		this.couleur = couleur;
		this.puissance = puissance;
		this.prix = prix;
		this.anneeFabrication = anneeFabrication;
	}
	
	public void setMarque(String marque)
	{
		this.marque = marque;
	}
	
	public void setModele(String modele)
	{
		this.modele = modele;
	}
	
	public void setCouleur (String couleur)
	{
		this.couleur = couleur;
	}
	
	public void setPrix (int prix)
	{
		this.prix = prix;
	}
	
	public void setAnneFabrication (int anneeFabrication)
	{
		this.anneeFabrication = anneeFabrication;
	}
	
	public String getMarque()
	{
		return this.marque;
	}
	
	public String getModele()
	{
		return this.modele;
	}
	
	public String getCouleur()
	{
		return this.couleur;
	}
	
	public int getPuissance()
	{
		return this.puissance;
	}
	
	public int getPrix()
	{
		return this.prix;
	}
	
	public int getAnneeFabrication()
	{
		return this.anneeFabrication;
	}
	
	public String toString()
	{
		return "Marque: " + marque + " modele: " + modele + " couleur: " + couleur 
		+ " prix: " + prix + " puissance " + puissance + " anneeFabrication " + anneeFabrication;
	}
	
	
	
	
}
