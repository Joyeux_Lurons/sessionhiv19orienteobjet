import java.util.Vector;
import java.util.Scanner;

public class TestVector 
{

	public static void afficherVector(Vector <Auto> VectAutos)
	{
		System.out.println("Les voitures: ");
		for (int i = 0; i < VectAutos.size(); i++) 
		{
			System.out.println("Voiture " + (i+1) + " " + VectAutos.elementAt(i));
		}

	}

	public static void triAutoOrdreAlpha(Vector <Auto> VectAutos)
	{
		System.out.println("Tri alphab�tique du vecteur!");
		for (int j = 0; j < VectAutos.size() - 1; j++) 
		{
			for (int i = 0; i < VectAutos.size() - 1; i++) {
				//System.out.println(i + VectAutos.elementAt(i).getMarque());
				if(VectAutos.elementAt(i).getMarque().compareTo(VectAutos.elementAt(i+1).getMarque()) < 0)
				{ 
					// OK les deux voitures sont en ordre alphab�tique
				}
				else
				{ // Les voitures ne sont pas en ordre alphab�tique, on les change de place
					Auto temp;
					temp = VectAutos.elementAt(i);
					VectAutos.set(i, VectAutos.elementAt(i+1));
					VectAutos.set(i+1, temp);
				}
			}
		}
	}


	public static int binarySearch(Vector <Auto> TableauTrie, String value, int starting, int ending)
	{
		if (ending < starting)
		{
			return -1;
		}
		int mid = (starting + ending) / 2;

		if ((TableauTrie.elementAt(mid).getMarque()).compareTo(value) > 0 && starting <= ending)
		{
			ending = mid-1;
		}

		if ((TableauTrie.elementAt(mid).getMarque()).compareTo(value) < 0  && starting <= ending)
		{
			starting = mid + 1;
		}
		if ((TableauTrie.elementAt(mid).getMarque()).compareTo(value) == 0 /*&& starting < ending*/)
		{
			return mid; 
		}

		return binarySearch(TableauTrie, value, starting, ending);
	}


	public static void rechercheDichoRecu(Vector <Auto> TableauTrie)
	{
		Scanner sc = new Scanner(System.in);
		System.out.println("Recherche dichotomique dans le vecteur.");
		System.out.println("Veuillez entrer la marque � chercher: ");
		String marque = sc.nextLine(); 
		//int position = 
		int position = binarySearch(TableauTrie, marque, 0, (TableauTrie.size() - 1));

		if (position == -1)
		{
			System.out.println("La marque sp�cifi�e ne se trouve pas dans le vecteur");
		}
		else
		{
			System.out.println("La marque " + marque + " se trouve a la position " + (position+1));
		}
	}

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Vector <Auto> VectAutos = new Vector<>(5,4);

		System.out.println("Capacit� initiale: " + VectAutos.capacity());

		Auto auto1 = new Auto("Ford", "Focus","rouge",18000,136,2014);		
		Auto auto2 = new Auto("Honda", "Accord","blanc",25500,166,2011);
		Auto auto3 = new Auto("Chrysler", "Crossfire","noir",46000,215,2011);
		Auto auto4 = new Auto("Chevrolet", "Impala", "argent", 27000,211,2012);
		Auto auto5 = new Auto("Nissan", "Altima", "bleu", 28000,250, 2011);
		Auto auto6 = new Auto("Toyota", "Echo", "vert", 14000, 108,2014);
		Auto auto7 = new Auto("Volvo","V70","gris", 45000,208,2013);
		Auto auto8 = new Auto("Mercedes", "SLK", "or", 65000,268,2012);
		VectAutos.add(0, auto1);
		VectAutos.add(0, auto2);
		VectAutos.add(0, auto3);
		VectAutos.add(0, auto4);
		VectAutos.add(0, auto5);
		VectAutos.add(0, auto6);
		VectAutos.add(0, auto7);
		VectAutos.add(0, auto8);

		Scanner sc = new Scanner(System.in);

		System.out.println("Size du Vector apr�s ajout: " + VectAutos.size());
		afficherVector(VectAutos);

		// Ajout des deux derniers �l�ments 
		Auto auto9 = new Auto("Mazda", "Tribute", "noir", 25000, 153, 2011);
		Auto auto10 = new Auto("Subaru", "Legacy", "blanc", 30000, 175, 2014);
		VectAutos.add(auto9);
		VectAutos.add(auto10);

		int i =0;

		triAutoOrdreAlpha(VectAutos);  // Tri en ordre alphab�tique du vecteur
		afficherVector(VectAutos); 
		rechercheDichoRecu(VectAutos); // Recherche dichotomique r�cursive 

		for (int j = 0; j < 3; j++) 
		{
			System.out.println("De quelle voiture voulez-vous changer la couleur (1-10)? ");
			i = sc.nextInt(); sc.nextLine();
			System.out.println("Quelle nouvelle couleur voulez-vous mettre? ");
			String couleur = sc.nextLine();
			VectAutos.elementAt((i-1)).setCouleur(couleur);
			afficherVector(VectAutos);
		}

		System.out.println("Taille du vecteur: " + VectAutos.size());
	}
}
