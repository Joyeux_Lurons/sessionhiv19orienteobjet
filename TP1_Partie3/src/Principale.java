
import java.util.Scanner;

public class Principale {
	
	//public static 
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Valider l'ordre
		Scanner sc = new Scanner(System.in);

		int ordre = 0;
		char choix = 'o';

		while (choix == 'o')
		{
			while (ordre % 2 == 0)
			{
				System.out.print("Veuillez entrer l'ordre du carr� magique [3-11 impair]: ");
				ordre = sc.nextInt();
			}
			
			CarreMagique cm = new CarreMagique(ordre);
			cm.remplirCarre(); // Remplir carr� 
			cm.afficheCarre();

			System.out.print("Voulez-vous recommencer? (o/n) ");
			choix = sc.next().charAt(0);
			ordre = 0;
		}
		System.out.println("Au revoir!");
	}
}
