
import java.lang.Math.*;

public class CarreMagique {
	
	private int cm[][], ordre, constanteMagique;
	
	CarreMagique(int nb) // Constructeur de la classe CarreMagique
	{
		ordre = nb;
		cm = new int[ordre][ordre];
		
		constanteMagique = ((ordre * ((int)Math.pow(ordre,2) + 1)) / 2); // On calcule la constante magique
		
		// Valable seulement pour les valeurs impaires 
		// On remplis le tableau de z�ros
		for (int i = 0; i < ordre; i++) {
			for (int j = 0; j < ordre; j++) {
				cm[i][j] = 0;
			}
		}

	}
	

	public void remplirCarre()
	{	
		/*
		 * On initialise le carr� � z�ro (d�j� fait) 														x	
		 * Donc: on mets 1 sur la case du milieu de la derni�re rang�e du bas								x		
		 * On choisis une autre case selon la r�gle suivante:										
		 * Un mouvement vers la case de droite (j+=1)
		 * Un mouvement vers la case du bas (i+=1)
		 * SI la case atteinte est libre -c-a-d �gale � z�ro (donc on a mis en pratique les cas de d�passement)
		 * 		On y place le nombre cm[i][j] = nb;
		 * SINON --La case est d�j� remplie (cm[case_i][case_j])
		 * 		On en choisis une autre en faisant un d�placement vers le bas � partir de la case de d�part (i-=1)
		 */
		
		int i, j = 0;

		i = (ordre-1);
		j = (((ordre)/2));
		cm[i][j] = 1;
		
		
		int nvellecase_i = i+1;
		int nvellecase_j = j+1;
		int valeur = 2; 
		
		
		while (valeur <= ordre * ordre)
		{
			if (nvellecase_j > (ordre-1)) // TEST des cas de d�passement des bornes vers la droite
			{
				nvellecase_j = 0;
			}
			if (nvellecase_i > (ordre-1)) // IDEM 		"		"		"		"
			{
				nvellecase_i = 0;
			}
			if (cm[nvellecase_i][nvellecase_j] == 0)
			{
				cm[nvellecase_i][nvellecase_j] = valeur;
				i = nvellecase_i;
				j = nvellecase_j;
				nvellecase_i = i+1;
				nvellecase_j = j+1;
			}
			else
			{
				nvellecase_i = i-1;
				nvellecase_j = j;
				cm[nvellecase_i][nvellecase_j] = valeur;
				i = nvellecase_i;
				j = nvellecase_j;
				nvellecase_i = i+1;
				nvellecase_j = j+1;	
			}
			valeur++;
		}

	}

	public void afficheCarre() {
		
		System.out.println("L'ordre du carr� est de " + ordre); // Informations du carr� magique
		System.out.println("La constante magique pour ce carr� est de: " + constanteMagique);
		System.out.println("La somme d'une ligne est de: " + calcSommeLig());
		System.out.println("La somme d'une colonne est de: " + calcSommeCol());
		System.out.println("La somme d'une diagonale est de: " + calcSommeDiag());
		System.out.println();
		
		for (int i = 0; i < ordre; i++) {
			for (int j = 0; j < ordre; j++) {
				System.out.print(cm[i][j]+ " \t");
			}
			System.out.println("\n");	
		}
	}

	private int calcSommeLig() {
		
		int sommeLigne = 0;
		
		for (int j = 0; j < ordre; j++) {
			sommeLigne += cm[0][j];
		}
		
		return sommeLigne;
	}

	private int calcSommeCol() {
		
		int sommeCol = 0;
		
		for (int i = 0; i < ordre; i++) {
			sommeCol += cm[i][0];
		}
		
		return sommeCol;
	}

	private int calcSommeDiag() {
		
		int sommeDiag = 0;
		int i = 0;
		
		for (int j = 0; j < ordre; j++, i++) {
			
			sommeDiag += cm[i][j];
		}
		
		return sommeDiag;
	}

}
