// comment trier un tableau des objets COMPARABLES ?
public class Tri 
// Utiliser la m�thode plusPetit() de l'interface Comparable
{ 
	public static void trier(Comparable[] tableau) 
	{
		Comparable tempo;
		
		for (int j = 0; j < tableau.length; j++) {
			
			for (int i = 0; i < tableau.length - 1; i++) 
			{
				if (!tableau[i].plusPetit(tableau[i+1]))
				{
					tempo = tableau[i+1];
					tableau[i+1] = tableau[i];
					tableau[i] = tempo;
				}
			}
		}
		//Votre code

	}
}
