public class RectInterf implements Comparable
{   
	// attributs (champs de donn�es, membres de donn�es)
	private int longueur, largeur;
	
	public RectInterf() 
	{}
	public RectInterf(int lo, int largeur) 
	{ 
		this.longueur = lo;
		this.largeur = largeur;
	}
	public RectInterf(int c) 
	{
		this(c, c);
	}
	// On s'engage � IMPL�MENTER la m�thode "plusPetit" (par ex. via surface)
	public boolean plusPetit(Object r) 
	{
		if ((this.longueur * this.largeur) < ((RectInterf) r).getLongueur() * ((RectInterf) r).getLargeur())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public String toString() {
		return "<Longueur : " + longueur + ", " + largeur +  " surface : " + (largeur*longueur) + ">";
	}
	
	public int getLongueur() {
		return longueur;
	}
	public void setLongueur(int longueur) {
		this.longueur = longueur;
	}
	public int getLargeur() {
		return largeur;
	}
	public void setLargeur(int largeur) {
		this.largeur = largeur;
	}

	
}
