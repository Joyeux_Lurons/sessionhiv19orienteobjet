public class PersInterf implements Comparable 
{ 

	public void setTaille(double taille) {
		this.taille = taille;
	}

	private double taille, poids;
	private char sexe;
	public PersInterf(double t, double p, char s) 
	{
		this.taille = t;
		this.poids = p;
		this.sexe = s;
	}

	// On s'engage � impl�menter la m�thode "plusPetit" :
	public boolean plusPetit(Object p) 
	{
		if (taille < ((PersInterf) p).getTaille())
		{
			return true;
		}
		return false;
	}

	public String toString() 
	{
		return sexe + " mesure " + taille + " metre et pese " + poids + " kgs";
	}


	public double getTaille() {
		return taille;
	}
}