import java.io.*;
import java.text.DecimalFormat;
public class Personne
{ 
  public static final int LONG_NP = 30 ; // 30 caract�res pour nom et pr�nom
  private String nomPre ;
  private char sexe ;
  private double taille, poids ;

  
    public static String lireChaine(String message) throws IOException {
    	
	  BufferedReader entree = new BufferedReader ( new InputStreamReader(System.in));
	  System.out.print(message);
	  return entree.readLine();
	}

    public static int lireEntier(String message) throws IOException {
	  String chaine = lireChaine(message);
	  return Integer.parseInt(chaine);
	  
	}
	
	public static double lireReel(String message) throws IOException {
	  String chaine = lireChaine(message);
	  return Double.valueOf(chaine).doubleValue();
	}
	
	public static char lireCaractere(String message) throws IOException {
	    	 	  
	  return lireChaine(message).toUpperCase().charAt(0);  	  
	}
	//Constructeurs
  public Personne() { 
	  nomPre = "Dumas-Lavoie F�lix";
	  sexe = 'm';
	  taille = 1.50;
	  poids = 150;
  }

  public Personne(String message) throws IOException
  {
	  System.out.println(message + "\n");
	  nomPre = lireChaine("Entrez le nom et prenom ").toUpperCase();
	  for (int i = nomPre.length()+1 ; i <= LONG_NP ;i++)
		    nomPre = nomPre + " ";
	  sexe = lireCaractere("Entrez f ou m pour le sexe ");
	  taille = lireReel("Entrez la taille en metre ");
	  poids = lireReel("Entrez le poids en kgs ");
  }
  
  public Personne(String np, char s, double t, double p) {
      nomPre = np.toUpperCase();
	  for (int i = nomPre.length()+1 ; i <= LONG_NP ;i++)
		    nomPre = nomPre + " ";
	  sexe = s ;
	  taille = t;
	  poids = p;
  }

  public void copier(Personne autrePers) {
      nomPre = autrePers.nomPre ;
	  sexe = autrePers.sexe ;
	  taille = autrePers.taille;
	  poids = autrePers.poids;  
  }
    
    public Personne(Personne autrePers) {
      copier(autrePers);
  }
    
  
  // quelques accesseurs :  
  public double getTaille()
  {
	  return taille;
  }
  
  public double getPoids()
  {
	  return poids;
  }
  public String getNom()
  {
	  return nomPre;
  }
  public char getSexe()
  {
	  return sexe;
  }
  
  // quelques modificateurs :  
  public void setTaille(double taiLLe)
  {
	taille = taiLLe;  
  }
  public void setPoids(double poiDs)
  {
	  poids = poiDs;
  }
  
  public void setNom(String nom)
  {
	  nomPre = nom;
  }
  public void setSexe(char sxe)
  {
	  sexe = sxe;
  }
    
  public void afficher() {
	  DecimalFormat fmt = new DecimalFormat(" ####.00 ");
	  System.out.print(nomPre);
   	  System.out.print((sexe == 'F') ? " feminin " : " masculin");
	  
	  System.out.print(" " + fmt.format(taille) + " ");
	  System.out.println(fmt.format(poids));  
  }  
  public void afficher(String qui) {
	  System.out.println("\nInformations sur la personne " + qui);
	  System.out.println("   Nom et prenom   : " + nomPre);
   	  System.out.println("   Sexe            : " +
						 ((sexe == 'F') ? "feminin" : "masculin"));
	  System.out.println("   Taille          : " + taille + " metre ");
	  System.out.println("   Poids           : " + poids + " kgs ");  
  }  
}
