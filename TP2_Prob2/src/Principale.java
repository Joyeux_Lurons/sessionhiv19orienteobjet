import java.util.Vector;


public class Principale {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		final int NBPERS = 10;
		final int INDICENVELLEPERS = 2;
		Personne pers [] = new Personne[NBPERS];
		
		for (int i = 0; i < pers.length; i++) {
			pers[i] = new Personne();
		}
		
		
		Vector<Personne> vectorPers = new Vector <Personne>();
		
		for (int i = 0; i < NBPERS; i++) {
			vectorPers.add(pers[i]);
		}
		System.out.print("Nombre de personnes contenues dans le vecteur: ");
		System.out.println(vectorPers.size());
		
		for (int i = 0; i < NBPERS; i++) {
			System.out.print("Numero: " + (i+1) + " ");
			vectorPers.elementAt(0).afficher();
		}
		// Ins�rer une personne � l'index 2 du vecteur
		System.out.println("INSERTION DANS LE VECTEUR ");
		vectorPers.set(2, new Personne("Nouvelle Personne ins�r�e",'f', 1.50,150));
		
		System.out.println("Deuxieme version");
		System.out.print("Nombre de personnes contenues dans le vecteur apr�s insertion: ");
		System.out.println(vectorPers.size());
		
		for (int i = 0; i < NBPERS; i++) {
			System.out.print("Numero: " + (i+1) + " ");
			vectorPers.elementAt(i).afficher();
		}
		
		
		System.out.println(" ");
		System.out.print("La quatrieme personne du vecteur est ");
		vectorPers.elementAt(3).afficher();
		
		System.out.println("Suppression de la personne 0. ");
		System.out.println("Contenu du vecteur apr�s suppression: ");
		vectorPers.remove(0);
		
		for (int i = 0; i < NBPERS - 1; i++) {
			System.out.print("Numero: " + (i+1) + " "); 
			vectorPers.elementAt(i).afficher();
		}
		
	}

}
