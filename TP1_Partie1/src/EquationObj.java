import java.util.Scanner;

public class EquationObj {


	public static void lireTermesEquation(EquationPublic eq, EquationPrivee eq2) 
	{
		Scanner sc = new Scanner(System.in);
		int a,b,c;

		int test = 0;
		eq.a = -1;					// On mets des valeurs bidon dans l'�quation
		eq.b = -1;
		eq.c = -1;
		eq2.set_equation(-1, -1, -1);
		
		while (eq.B2Moins4acNegatif())
		{
			if (test > 0)
			{
				System.out.println("Veullez entrer des valeurs a b c qui donnerons une racine positive ");
			}
			System.out.println("Terme a: ");
			a = sc.nextInt();
			System.out.println("Terme b: ");
			b = sc.nextInt();
			System.out.println("Terme c: ");
			c = sc.nextInt();
			eq.a = a;
			eq.b = b;
			eq.c = c;
			eq2.set_equation(a, b, c);
			test++;
		}	
		
	}


	public static void afficherEquationPublic(EquationPublic eq)
	{
		System.out.println("Equation avec atributs Public");
		System.out.println("(-b+Racine(b^2 -4ac)/(2a))");
		System.out.println("Terme a: " + eq.a);
		System.out.println("Terme b: " + eq.b);
		System.out.println("Terme c: " + eq.c);
		System.out.println("Resultat ((b^2)-4ac): " + eq.b2Moins4ac());
		System.out.println("Resultat equation R1: " + eq.r1);
		System.out.println("Resultat equation R2: " + eq.r2);
	}

	public static void afficherEquationPrivee(EquationPrivee eq)
	{
		System.out.println("Equation avec atributs Priv�es");
		System.out.println("(-b+Racine(b^2 -4ac)/(2a))");
		System.out.println("Terme a: " + eq.getA());
		System.out.println("Terme b: " + eq.getB());
		System.out.println("Terme c: " + eq.getC());
		System.out.println("Resultat ((b^2)-4ac): " + eq.b2Moins4ac());
		System.out.println("Resultat equation R1: " + eq.getRacine1());
		System.out.println("Resultat equation R2: " + eq.getRacine2());
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		String choix = " ";
		
		while (choix.charAt(0) != 'n')
		{
			choix = " ";
			System.out.println("Choisissez votre type d'�quation");
			System.out.println("1. Equation avec attributs publics");
			System.out.println("2. Equation avec attributs priv�s");
			choix = sc.nextLine();
			
			EquationPublic UneEquation = new EquationPublic();
			EquationPrivee UneAutreEquation = new EquationPrivee();

			lireTermesEquation(UneEquation,UneAutreEquation);
			System.out.println("");

			if (choix.charAt(0) == '1')
			{
				UneEquation.calculerRacines();
				afficherEquationPublic(UneEquation);
			}
			else
			{
				UneAutreEquation.calculerRacines();
				afficherEquationPrivee(UneAutreEquation);
			}
			
			System.out.println("2. Voulez-vous recommencer? (o/n) ");			
			choix = sc.nextLine();
		}
	}
}
