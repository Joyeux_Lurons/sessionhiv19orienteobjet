import java.lang.Math.*;

public class EquationPrivee {
	
	private int a,b,c;
	private double  r1, r2;
	
	EquationPrivee() // Constructeur implicite de la classe 
	{
		
	}
	
	public void set_equation(int valA,int valB,int valC)
	{
		a = valA;
		b = valB;
		c = valC;
	}
	
	public int getA()
	{
		return a;	
	}
	public int getB()
	{
		return b;
	}
	
	public int getC()
	{
		return c;
	}
	
	public double getRacine1() 
	{
		return r1;
	}
	
	public double getRacine2()
	{
		return r2;
	}
	
	public void calculerRacines()
	{
		r1 = (((-(b)) + Math.sqrt(b2Moins4ac())) / ((2*a)));
		r2 = (((-(b)) - Math.sqrt(b2Moins4ac())) / ((2*a)));
	}	
	
	public double b2Moins4ac() {
		return (Math.pow(b, 2)- (4*a*c));
	}
	
	public boolean testB2Moins4acPositif()
	{
		if ( b2Moins4ac() > 0)
			return true;
		else
			return false;
	}
}
