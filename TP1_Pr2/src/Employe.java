public class Employe 
{

  String nom;
  String numero;
  double salaireHoraire;
  double nbreHeuresSemaine;
  int anciennete;
  
  
  public final double ASSURANCEEMPLOI = 0.111;
  public final double FONDSPENSION = 0.0136;
  public final double COTISATIONSYNDICALE = 20;
  public final double TAUXPROVINCIAL = 0.20;
  public final double TAUXFEDERAL = 0.19;
  

  public Employe()
  {
  
  }
  

  public double salaireBrut()
  {
	  return (nbreHeuresSemaine * salaireHoraire);
  }

  
  public double salaireNetAvantImpot()
  {
	  return (salaireBrut() - (ASSURANCEEMPLOI * salaireBrut()) - (FONDSPENSION * salaireBrut()) - COTISATIONSYNDICALE);
  }

  
  public double impotFederal()
  {
	 return (salaireNetAvantImpot() * TAUXFEDERAL);
  }

  
  public double impotProvincial()
  {
	 return (salaireNetAvantImpot() * TAUXPROVINCIAL);
  }
  
  
  public double salaireNetApresImpot()
  {
	  return (salaireNetAvantImpot() - impotFederal() - impotProvincial());
  }

  
  public int joursVacances()
  {
	  switch (numero.charAt(0))
	  {
		  case '1':
			  return 5 + anciennete;
		  case '2':
			  return 10 + anciennete;
		  case '3':
			  return 15 + anciennete;
		  case '4':
			  return 20 + anciennete;
		  default:
			  return anciennete;
	  }
  }

  public void heuresSup (double nbreHeuresSup)
  {
	  nbreHeuresSemaine += nbreHeuresSup;
  }
}