import java.io.*;
import java.text.DecimalFormat;
import java.util.Scanner;

public class TestEmploye 
{
	public static void lireInfosEmployee(Employe UnEmploye)
	{
		 Scanner sc = new Scanner(System.in);
		 
		 System.out.println("Nom de l'employ�-e?");
		 UnEmploye.nom = sc.nextLine();
		 
		 System.out.println("Numero de l'employ�?");
		 UnEmploye.numero = sc.nextLine();
		 
		 System.out.println("Nb. d'heures travaill�es durant la semaine ?");
		 UnEmploye.nbreHeuresSemaine = sc.nextDouble();
		 
		 System.out.println("Heures suppl�mentaires ?");
		 UnEmploye.heuresSup(sc.nextDouble());
		 
		 System.out.println("Salaire horaire ?");
		 UnEmploye.salaireHoraire = sc.nextDouble();
		 
		 System.out.println("Quelle est l'anciennet� ?");
		 UnEmploye.anciennete = sc.nextInt();
		 
	}
	
	
	public static void afficherInfosEmployee(Employe UnEmploye) 
	{
		 DecimalFormat df = new DecimalFormat("#.00");
		 System.out.println("-------------------------------");
		 System.out.println("Informations de l'employ�-e");
		 System.out.println("-------------------------------");
		 System.out.println("Salaire brut: " + UnEmploye.salaireBrut() + '$');
		 System.out.println("Salaire net avant imp�t: " + UnEmploye.salaireNetAvantImpot() + '$');
		 System.out.println("Impot f�d�ral: " + df.format(UnEmploye.impotFederal()) + '$');
		 System.out.println("Impot provincial: " + df.format(UnEmploye.impotProvincial()) + '$');
		 System.out.println("Salaire net apr�s imp�t: " + df.format(UnEmploye.salaireNetApresImpot()) + '$');
		 System.out.println("Jours vacances: " + df.format(UnEmploye.joursVacances()) + " jours");
		 System.out.println("-------------------------------");	
	}
	 

public static void main(String[] args) throws IOException
{

 Employe UnEmploye = new Employe();
 lireInfosEmployee(UnEmploye);
 afficherInfosEmployee(UnEmploye);
 
}


}