package tests;

import noeuds.NoeudListe;

import objets.ClasseObjet;

public class TestListes {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
// CR�ATION DU PREMIER NOEUD DE LA LISTE 
		
		ClasseObjet objet1 = new ClasseObjet("Lion");
		NoeudListe nouvelleListe = new NoeudListe(objet1);
		
			// Affichage d'un �l�ment de la liste
			System.out.println("Affichage du premier �l�ment:");
			nouvelleListe.toString();
	
// AJOUT DANS LA LISTE		
	
		// Ajout en t�te de liste
		System.out.println("Ajout en t�te de liste");
		ClasseObjet objet2 = new ClasseObjet("Veau");
		nouvelleListe.ajoutTeteListe(objet2, nouvelleListe);
		
			// Affichage de la liste
			System.out.println("Affichage de la liste compl�te:");
			nouvelleListe.afficherListe();

		// Insertion en cours de liste 
		System.out.println("Ajout en cours de liste");
		ClasseObjet objet4 = new ClasseObjet("Vache");
		nouvelleListe.mettreSuivant(objet4);

			// Affichage de la liste
			System.out.println("Affichage de la liste compl�te:");
			nouvelleListe.afficherListe();
		
		
		// Ajout � la fin de la liste 
		System.out.println("Ajout � la fin de la liste");
		ClasseObjet objet3 = new ClasseObjet("Crabe",6,2, true);
		nouvelleListe.ajoutFinListe(objet3);
		
			// Affichage de la liste
			System.out.println("Affichage de la liste compl�te:");
			nouvelleListe.afficherListe();

			
/////////////////// RECHERCHE DANS LA LISTE
			System.out.println("Recherche dans la liste");
			if (nouvelleListe.rechercheElementListe(objet3))
			{
				System.out.println("L'objet " +nouvelleListe.getSuivant().getSuivant().getSuivant().getObject().getAnimal() + " existe dans la liste ");
			}
			else 
			{
				System.out.println("L'objet n'est pas dans la liste ");
			}
		
			
			
			
//////////// SUPPRESSIONS DANS LA LISTE
		
		// Suppression en cours de liste
		System.out.println("Suppression en cours de liste");
		nouvelleListe.getSuivant().suppresionEnCoursListe();
		
			//Affichage de la liste
			System.out.println("Affichage de la liste compl�te:");
			nouvelleListe.afficherListe();
		
		// Suppression en t�te de liste
		System.out.println("Suppression en t�te de liste");
		nouvelleListe.suppressionEnTetedeListe(nouvelleListe);
			
			//Affichage de la liste
			System.out.println("Affichage de la liste compl�te:");
			nouvelleListe.afficherListe();
	
		// Suppression en fin de liste
		System.out.println("Suppression en fin de liste");		
		nouvelleListe.suppressionEnFinDeListe(nouvelleListe);
		System.out.println("Affichage de la liste compl�te:");
		nouvelleListe.afficherListe();
			
		// Appels des m�thodes de la classe objet: 
		nouvelleListe.getObject().criDeLanimal();
		
		
	}

}
