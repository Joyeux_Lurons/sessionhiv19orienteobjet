package objets;

public class ClasseObjet {

	private String animal;
	private int nbPattes;
	private int nbYeux;
	private boolean exoSquelette;
	
	
	public ClasseObjet ()
	{
		this.animal = "Lion";
		this.nbPattes = 4;
		this.nbYeux = 2;
		this.exoSquelette = false;
	}
	public ClasseObjet (String animal)
	{
		this.animal = animal;
		this.nbPattes = 4;
		this.nbYeux = 2;
		this.exoSquelette = false;
	}
	
	public ClasseObjet(String typeAnimal, int nbPattes, int nbYeux, boolean exoSquelette)
	{
		this.animal = typeAnimal;
		this.nbPattes = nbPattes;
		this.nbYeux = nbYeux;
		this.exoSquelette = exoSquelette;
	}
	
	public void criDeLanimal()
	{
		System.out.println("Le " + animal + " fait un son ou un mouvement le caract�risant!");
		
	}
	
	public String getAnimal() 
	{
		return this.animal;
	}
	
	public String toString()
	{
		String test;
		
		if (this.exoSquelette)
		{
			test = " Il poss�de un exosquelette";
		}
		else
		{
			test = " Il ne poss�de pas d'exosquelette";
		}
		return " Animal " + this.animal + " poss�de " + this.nbPattes 
				+ " pattes, " + this.nbYeux + " yeux. " + test;
	}
	
	public void setAnimal(String typeAnimal)
	{
		this.animal = typeAnimal;
	}
	
	public void setNbPattes(int nbPattes)
	{
		this.nbPattes = nbPattes;
	}
	
}
