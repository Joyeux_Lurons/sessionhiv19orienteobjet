package noeuds;

import noeuds.*;
import objets.ClasseObjet;


public class NoeudListe 
{
	private ClasseObjet donnee;
	private NoeudListe suivant;
	
	NoeudListe(ClasseObjet obj, NoeudListe noeudSuivant)
	{
		donnee = obj;
		suivant = noeudSuivant;
	}
	
	public NoeudListe(ClasseObjet obj)
	{
		this(obj,null);
	}
	
	// Ajout en cours de liste 
	public void mettreSuivant(ClasseObjet obj)
	{	
		NoeudListe prochainNoeud = new NoeudListe(obj);
		prochainNoeud.suivant = this.suivant;
		setSuivant(prochainNoeud);
	}
	
	// Ajout au d�but de la liste
	public void ajoutTeteListe(ClasseObjet obj, NoeudListe noeudInitial)
	{
		NoeudListe ancienNoeud = new NoeudListe(noeudInitial.donnee);
		noeudInitial.suivant = ancienNoeud;
		noeudInitial.donnee = obj;
	}
	
	// Ajout � la fin de la liste 
	public void ajoutFinListe(ClasseObjet obj)
	{
		if (this.suivant != null)
		{
			suivant.ajoutFinListe(obj);
		}
		else
		{
			NoeudListe prochainNoeud = new NoeudListe(obj);
			setSuivant(prochainNoeud);
		}
	}
	
	// Suppression en cours de liste 
	public void suppresionEnCoursListe()
	{
		if (this.suivant != null)
		{
			this.donnee = this.suivant.donnee;
			this.suivant = this.suivant.suivant;
		}
		else
		{
			this.donnee = null;
			this.suivant = null;
		}
	}
	
	// Suppression en tete de liste	
	public void suppressionEnTetedeListe(NoeudListe NoeudInitial)
	{
		NoeudInitial.donnee = NoeudInitial.suivant.donnee;
		NoeudInitial.suivant = NoeudInitial.suivant.suivant;
	}
	
	// Suppression en fin de liste
	public void suppressionEnFinDeListe(NoeudListe NoeudInitial)
	{
		if(NoeudInitial.suivant == null) // Si il y a un seul �l�ment dans la liste
		{
			NoeudInitial.donnee = null;
			NoeudInitial.suivant = null;
		}
		if (NoeudInitial.suivant.suivant == null) // Sinon on v�rifie 
		{
			NoeudInitial.suivant.donnee = null;
			NoeudInitial.suivant = null;
		}
		else
		{
			suppressionEnFinDeListe(NoeudInitial.suivant);
		}
	}
	
	// Recherche d'un �l�ment dans la liste
	
	public boolean rechercheElementListe(ClasseObjet obj)
	{
		if (this.donnee.equals(obj))
		{
			return true;
		}
		else if (suivant != null)
		{
			return this.suivant.rechercheElementListe(obj);
		}
		return false;
	}
	
	public void setSuivant(NoeudListe autreNoeud)
	{
		this.suivant = autreNoeud;
	}
	
	public ClasseObjet getObject()
	{
		return donnee;
	}
	
	public NoeudListe getSuivant()
	{
		return suivant;
	}

	
	public String toString()
	{
		String ch = " Un element de valeur :  "+ donnee; 	
		System.out.println(ch);
		return ch;
	}
	
	public void afficherListe()
	{
		this.toString();
		if (this.suivant != null)
		{
			this.suivant.afficherListe();
		}
	}
}

