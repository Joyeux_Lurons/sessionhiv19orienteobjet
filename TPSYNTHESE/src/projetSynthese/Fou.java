package projetSynthese;

public class Fou extends Piece {

	public Fou(String nom, String couleur) {
		super(nom, couleur);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean estValide(Position depart, Position arrivee) {
		// TODO Auto-generated method stub
		if (Math.abs(depart.getColonne() - arrivee.getColonne()) == Math.abs(depart.getLigne() - arrivee.getLigne()))
		{
			return true;
		}
		return false;
	}
	
}
