package projetSynthese;
import java.awt.Dimension;
import java.awt.Rectangle;

import javax.swing.border.EtchedBorder;
import java.awt.GridLayout;
import java.awt.*;
import javax.swing.*;

import java.awt.event.*;

public class FenetreJeu extends JFrame
{
	Echiquier e;        //echiquier
	JLabel [][] tab;    //tableau de JLabels

	JPanel jPanel1 = new JPanel();  // panel du haut
	JPanel jPanel2 = new JPanel();  // panel du bas ( grille )
	GridLayout gridLayout1 = new GridLayout();

	JButton boutonDebuter = new JButton();
	JTextField champTexte = new JTextField();

	public FenetreJeu()   // constructeur appelle m�thode JBInit
	{
		try
		{
			jbInit();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

	}

	private void jbInit() throws Exception
	{


		tab = new JLabel[8][8];   // cr�ation du tableau de JLabel
		e = new Echiquier();      // cr�ation de l'�chiquier


		this.getContentPane().setLayout(null);
		this.setSize(new Dimension(568, 585));
		this.setTitle("Jeu d'Echecs");
		jPanel1.setBounds(new Rectangle(5, 10, 550, 45));
		jPanel1.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
		jPanel1.setLayout(null);
		jPanel2.setBounds(new Rectangle(5, 65, 550, 465));
		jPanel2.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
		jPanel2.setLayout(gridLayout1);
		gridLayout1.setColumns(8);
		gridLayout1.setRows(8);
		this.getContentPane().add(jPanel2, null);
		jPanel1.add(champTexte, null);
		jPanel1.add(boutonDebuter, null);
		this.getContentPane().add(jPanel1, null);


		boutonDebuter.setBounds(new Rectangle(15, 10, 130, 25));
		boutonDebuter.setText("DEBUTER");


		champTexte.setBounds(new Rectangle(160, 10, 320, 25));



		// les �couteurs
		GestionnaireEvenement gest = new GestionnaireEvenement();
		boutonDebuter.addMouseListener(gest);


		for ( int i = 0; i <8; i++ )
		{
			for ( int j = 0; j <8 ; j++ )
			{
				tab[i][j] = new JLabel(); // cr�ation du JLabel
				jPanel2.add(tab[i][j]);  // ajouter au Panel
				tab[i][j].setOpaque(true);
				tab[i][j].setHorizontalAlignment(SwingConstants.CENTER);  // pour que les pieces apparaissent au centre de la case
				tab[i][j].addMouseListener(gest);  // ajouter l'�couteur aux sources


				if ( (i+j) % 2 == 0 )
					tab[i][j].setBackground(Color.lightGray );  //couleur des cases
				else
					tab[i][j].setBackground(Color.darkGray );
			}
		}


	}
	// main pour pouvoir ex�cuter l'interface graphique
	public static void main ( String [] args )
	{
		FenetreJeu j = new FenetreJeu();
		j.setVisible(true);
		j.setLocation(100, 130);
		j.setDefaultCloseOperation(EXIT_ON_CLOSE);  // ferme le processus associ�

	}


	// classe priv�e (interne) pour la gestion d'�v�nements
	private class GestionnaireEvenement extends MouseAdapter
	{

		Piece pieceTampon;
		ImageIcon iconeTampon;
		int ligneClic;
		int colonneClic;
		Position depart, arrivee;
		String couleurControle = "blanc";
		public void mouseClicked(MouseEvent eve)
		{
			if ( eve.getSource() == boutonDebuter )
			{
				e.debuter();
				tab[0][0].setIcon ( new ImageIcon ("Icones/TB.gif"));
				tab[0][1].setIcon ( new ImageIcon ("Icones/CB.gif"));
				tab[0][2].setIcon ( new ImageIcon ("Icones/FB.gif"));
				tab[0][3].setIcon ( new ImageIcon ("Icones/RB.gif"));
				tab[0][4].setIcon ( new ImageIcon ("Icones/DB.gif"));
				tab[0][5].setIcon ( new ImageIcon ("Icones/FB.gif"));
				tab[0][6].setIcon ( new ImageIcon ("Icones/CB.gif"));
				tab[0][7].setIcon ( new ImageIcon ("Icones/TB.gif"));
				tab[7][0].setIcon ( new ImageIcon ("Icones/TN.gif"));
				tab[7][1].setIcon ( new ImageIcon ("Icones/CN.gif"));
				tab[7][2].setIcon ( new ImageIcon ("Icones/FN.gif"));
				tab[7][3].setIcon ( new ImageIcon ("Icones/RN.gif"));
				tab[7][4].setIcon ( new ImageIcon ("Icones/DN.gif"));
				tab[7][5].setIcon ( new ImageIcon ("Icones/FN.gif"));
				tab[7][6].setIcon ( new ImageIcon ("Icones/CN.gif"));
				tab[7][7].setIcon ( new ImageIcon ("Icones/TN.gif"));

				//Pions
				for ( int i = 0; i <8; i++ )
				{
					tab[1][i].setIcon(new ImageIcon("Icones/PB.gif"));
					tab[6][i].setIcon(new ImageIcon("Icones/PN.gif"));
				}
				champTexte.setText(couleurControle);
			}
			else // donc a clique sur un Label
			{
				//trouver lequel
				for ( int i = 0; i < 8 ; i++ )
				{
					for ( int j = 0; j < 8; j++ )
					{
						if (eve.getSource() == tab[i][j])
						{
							ligneClic = i;
							colonneClic = j;
						}
					}
				}
				champTexte.setText(couleurControle);


				//SUIVRE LES �TAPES DU DOCUMENT FOURNI SUR L�A
				if (pieceTampon == null && e.getCase(ligneClic, colonneClic).estOccupee() && e.getCase(ligneClic, colonneClic).getPiece().getCouleur().equals(couleurControle))
				{
					depart = new Position(ligneClic,colonneClic);
					arrivee = new Position(ligneClic,colonneClic);
					pieceTampon = e.getCase(ligneClic, colonneClic).getPiece(); 	
					System.out.println(pieceTampon.getNom());
					System.out.println(ligneClic + " colonne: " + colonneClic);
					iconeTampon = (ImageIcon) tab[ligneClic][colonneClic].getIcon();
					tab[ligneClic][colonneClic].setIcon(null);
				}

				// Deuxi�me cas
				
				else if (pieceTampon != null  && !(e.getCase(depart.getLigne(), depart.getColonne()).getPiece().getNom().charAt(0) =='p' && (Math.abs(ligneClic - depart.getLigne()) == Math.abs(colonneClic - depart.getColonne()))))
				{
					System.out.println((Math.abs(ligneClic - depart.getLigne()) == Math.abs(colonneClic - depart.getColonne())));
					arrivee = new Position(ligneClic,colonneClic);
		
						if (e.getCase(depart.getLigne(), depart.getColonne()).getPiece().estValide(depart, arrivee))
						{
							if (e.cheminPossible(depart, arrivee))
							{
								e.getCase(arrivee.getLigne(), arrivee.getColonne()).ajouterPiece(pieceTampon);
								e.getCase(depart.getLigne(), depart.getColonne()).enleverPiece();
								tab[ligneClic][colonneClic].setIcon(iconeTampon);
								iconeTampon = null;
								pieceTampon = null;
								alterne();
								champTexte.setText(couleurControle);
							}	
							else
							{
								tab[depart.getLigne()][depart.getColonne()].setIcon(iconeTampon);
								iconeTampon = null;
								pieceTampon = null;
							}
						}
						else
						{
							tab[depart.getLigne()][depart.getColonne()].setIcon(iconeTampon);
							iconeTampon = null;
							pieceTampon = null;
						}	
				}

				// troisi�me cas
				else if (pieceTampon != null && e.getCase(arrivee.getLigne(), arrivee.getColonne()).estOccupee())
				{
					/*if (e.getCase(arrivee.getLigne(),arrivee.getColonne()).getPiece().estValide(depart, arrivee))
					{
						if (e.cheminPossible(depart, arrivee))
						{
							e.getCase(arrivee.getLigne(), arrivee.getColonne()).ajouterPiece(pieceTampon);
							e.getCase(depart.getLigne(), depart.getColonne()).enleverPiece();
							pieceTampon = null;
							e.getCase(depart.getLigne(), depart.getColonne()).enleverPiece();
							tab[depart.getLigne()][depart.getColonne()].setIcon(iconeTampon);
							alterne();
							champTexte.setText(couleurControle);
						}
					}*/
					 if (e.caputreParUnPionPossible(depart, arrivee))
					{
						e.getCase(depart.getLigne(), depart.getColonne()).enleverPiece();
						e.getCase(arrivee.getLigne(), arrivee.getColonne()).ajouterPiece(pieceTampon); 
						tab[arrivee.getLigne()][arrivee.getColonne()].setIcon(iconeTampon);
						iconeTampon = null;
						pieceTampon = null;
						alterne();
						champTexte.setText(couleurControle);
					}
				}

			}
		}//Fin m�thode mouseClicked()
		public void alterne ()
		{
			if (couleurControle == "blanc")
				couleurControle = "noir";
			else
				couleurControle = "blanc";
		}

	}//Fin classe interne
}//Fin FenetreJeu









