package projetSynthese;

public class Reine extends Piece {

	public Reine(String nom, String couleur) {
		super(nom, couleur);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean estValide(Position depart, Position arrivee) {
		// TODO Auto-generated method stub
		if (depart.getLigne() == arrivee.getLigne())
		{
			return true;
		}
		if (depart.getColonne() == arrivee.getColonne())
		{
			return true;
		}
		if (Math.abs(depart.getColonne() - arrivee.getColonne()) == Math.abs(depart.getLigne() - arrivee.getLigne()))
		{
			return true;
		}
		return false;
		
	}

}
