
package projetSynthese;

public class Echiquier implements MethodesEchiquier {
	private Case location [][];

	public Echiquier()
	{
		location = new Case[8][8];
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				location [i][j] = new Case();
			}
		}
	}

	//M�thode � compl�ter 
	public void debuter()
	{
		int k;
		for (k=0; k < 8; k++) 
		{
			// Pions 
			location[1][k].ajouterPiece(new Pion("p" + String.valueOf(k), "blanc"));
			location[6][k].ajouterPiece(new Pion("p" + String.valueOf(k), "noir"));
		}
		//Autres pi�ces    
		// Tours
		location[0][0].ajouterPiece(new Tour("t" + String.valueOf(0), "blanc"));
		location[0][7].ajouterPiece(new Tour("t" + String.valueOf(7), "blanc"));
		// Pi�ces noires
		location[7][0].ajouterPiece(new Tour("t" + String.valueOf(0), "noir"));
		location[7][7].ajouterPiece(new Tour("t" + String.valueOf(7), "noir"));

		// Cheval
		location[0][1].ajouterPiece(new Cheval("c" + String.valueOf(1), "blanc"));
		location[0][6].ajouterPiece(new Cheval("c" + String.valueOf(6), "blanc"));

		location[7][1].ajouterPiece(new Cheval("c" + String.valueOf(1), "noir"));
		location[7][6].ajouterPiece(new Cheval("c" + String.valueOf(6), "noir"));

		// Fou
		location[0][2].ajouterPiece(new Fou("f" + String.valueOf(2), "blanc"));
		location[0][5].ajouterPiece(new Fou("f" + String.valueOf(5), "blanc"));


		location[7][2].ajouterPiece(new Fou("f" + String.valueOf(2), "noir"));
		location[7][5].ajouterPiece(new Fou("f" + String.valueOf(5), "noir"));

		// Roi
		location[0][3].ajouterPiece(new Roi("k" + String.valueOf(3), "blanc"));
		location[7][3].ajouterPiece(new Roi("k" + String.valueOf(3), "noir"));

		// Reine 
		location[0][4].ajouterPiece(new Reine("r" + String.valueOf(4), "blanc"));
		location[7][4].ajouterPiece(new Reine("r" + String.valueOf(4), "noir"));  
	}

	public Case getCase ( int ligne, int colonne ) // Ok
	{
		return location[ligne][colonne];
	}

	@Override
	public boolean caputreParUnPionPossible(Position depart, Position arrivee) {

		boolean capture = false;
		Piece pieceDepart =  location[depart.getLigne()][depart.getColonne()].getPiece();
		Piece pieceArrivee = null;
		if (pieceDepart.getNom().charAt(0) == 'p' && location[arrivee.getLigne()][arrivee.getColonne()].estOccupee())
		{
			pieceArrivee = location[arrivee.getLigne()][arrivee.getColonne()].getPiece();

			if (arrivee.getColonne() == depart.getColonne() + 1 || arrivee.getColonne() == depart.getColonne() -1 && !pieceDepart.getCouleur().equals(pieceArrivee.getCouleur()))
			{
				if (pieceDepart.getCouleur().equals("noir"))
				{
					if (arrivee.getLigne() == depart.getLigne() - 1)
					{
						capture = true;
					}
				}
				else
				{
					if (arrivee.getLigne() == depart.getLigne() + 1)
					{
						capture = true; 
					}
				}
			}

			// D�placement gauche / droite de 1


		}
		// d�placement en diagonale
		// Si la position de d�part ligne/colonne
		// Si le pion est noir d�placement BG BD 
		// Si le poin est blanc d�placement HG HD
		return capture;
	}



	//M�thode � compl�ter  
	// 8 cas possibles 
	public boolean cheminPossible (Position depart, Position arrivee)
	{
		int diffCol, diffLig, i;
		diffCol = arrivee.getColonne() - depart.getColonne();
		diffLig = arrivee.getLigne() - depart.getLigne();

		// Si le d�placement est vertical 

		if (depart.getLigne() == arrivee.getLigne() && depart.getColonne() == arrivee.getColonne())
		{
			return true;
		}
		else if (diffCol==0)
		{
			// D�placement par en haut 
			if (diffLig < 0)
			{
				// de la ligne de d�part � la colonne d'arriv�e 
				for (i = depart.getLigne()- 1; i > arrivee.getLigne(); i--) 
				{
					if (location[i][arrivee.getColonne()].estOccupee())
					{
						return false;
					}
				}
			}

			// D�placement par en bas
			if (diffLig > 0)
			{
				for (i = depart.getLigne() + 1; i < arrivee.getLigne(); i++) 
				{
					if (location[i][arrivee.getColonne()].estOccupee())
					{
						return false;
					}
				}	
			}
		}


		// D�placement horisontal
		else if (diffLig==0)			// Est de facto un chemin valide
		{		
			// d�placement � droite
			if (diffCol > 0)
			{
				for (i = depart.getColonne() + 1; i < arrivee.getColonne(); i++) 
				{
					if (location[arrivee.getLigne()][i].estOccupee())
					{
						return false;
					}
				}
			}
			// D�placement � gauche 
			if (diffCol < 0)
			{
				for (i = depart.getColonne() - 1; i > arrivee.getColonne(); i--) 
				{
					if (location[arrivee.getLigne()][i].estOccupee())
					{
						return false;
					}
				}
			}
		}

		// D�placement diagonal 
		else if (Math.abs(diffLig) == Math.abs(diffCol))
		{
			// HG 
			if (diffLig < 0 && diffCol < 0)
			{
				int j = depart.getLigne() - 1;
				for (i = depart.getColonne() - 1; i > arrivee.getColonne() -1; --i) // Ok
				{
					if (location[j][i].estOccupee()) 
					{
						return false;
					}
					--j;
				}			
			}
			// HD
			if (diffLig < 0 && diffCol > 0)
			{
				int j = depart.getLigne() - 1;
				for (i = depart.getColonne() +1; i < arrivee.getColonne(); ++i) 
				{				
					if (location[j][i].estOccupee())
					{
						return false;
					}
					--j;
				}	
			}

			// BG 
			if (diffLig > 0 && diffCol < 0)
			{
				int j = depart.getLigne() + 1;
				for (i = depart.getColonne() - 1; i > arrivee.getColonne(); --i) {
					if (location[j][i].estOccupee())
					{
						return false;
					}
					++j;
				}
			}
			// BD
			if (diffLig > 0 && diffCol > 0)
			{
				int z = depart.getColonne() + 1;
				for (int j = depart.getLigne() + 1; j < arrivee.getLigne(); j++) {
					if (location[j][z].estOccupee())
					{
						return false;
					}	
					z++;
				}
			}
		}		

		else if (depart.getLigne() == arrivee.getLigne() && depart.getColonne() == arrivee.getColonne())
		{
			return true;
		}
		//System.out.println(location[depart.getLigne()][depart.getColonne()].getPiece().getNom().charAt(0));

		/// V�rifier la position d'arriv�e 
		// dans la position d'arriv�e, est-ce que l'ariv�e est occup�e par une pi�ce? de quelle couleur?
		if (location[depart.getLigne()][depart.getColonne()].getPiece().getNom().charAt(0) !='p') // Si il y a une pi�ce � l'arriv�e  
		{	
			Piece p = null;
			if (location[arrivee.getLigne()][arrivee.getColonne()].estOccupee())
			{
				p = location[arrivee.getLigne()][arrivee.getColonne()].getPiece();
			}
			if (p == null)
			{
				return true;
			}
			else if (p.getCouleur().equals(location[depart.getLigne()][depart.getColonne()].getPiece().getCouleur()))
			{
				return false;
			}
			else
			{
				return true;
			}
		}

		else if (location[depart.getLigne()][depart.getColonne()].getPiece().getNom().charAt(0) == 'p')
		{

			if (!location[arrivee.getLigne()][arrivee.getColonne()].estOccupee()) // Si la case est vide le pion peut avancer
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		// M�thode est occup�e 
		// Est-ce que je suis un pion? Je ne peut pas manger avec mon d�placement normal si je suis un pion
		// Si le d�part c'est une piece blanche, si � l'arriv�e est une pi�ce blanche chemin non possible 
		// SINON true 

		return false;
	}

	/*public boolean roquePossible (Position depart, Position arrivee ) 
{ 

}

public boolean priseEnPassantPossible( Position depart, Position arrivee )
{
}

public boolean promotionPossible (Position depart, Position arrivee)
{
if ( location [depart.getLigne()][depart.getColonne()].getPiece().getCouleur() == "blanc" )*/


	/*	public static void main ( String [] args )
	{
		Echiquier e = new Echiquier ();
		e.debuter();
	}*/


}