package projetSynthese;

public class Pion extends Piece {

	public Pion(String nom, String couleur) {
		super(nom, couleur);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean estValide(Position depart, Position arrivee) 
	{
		if (depart.getColonne() == arrivee.getColonne() && depart.getLigne() == arrivee.getLigne())
		{
			return true;
		}
		else if (depart.getColonne() == arrivee.getColonne())
		{
			if (this.getCouleur().equals("blanc"))
			{
				if (depart.getLigne() == 1 && arrivee.getLigne() == 2 || arrivee.getLigne() == 3)
				{
					return true;
				}
				if (depart.getLigne() != 1 &&  (arrivee.getLigne() == depart.getLigne() + 1))
				{
					return true;
				}
			}

			if (this.getCouleur().equals("noir"))
			{
				if (depart.getLigne() == 6 && arrivee.getLigne() == 5 || arrivee.getLigne() == 4) // Premier mvt du pion
				{
					return true;
				}	
				if (depart.getLigne() != 6 &&  (arrivee.getLigne() == depart.getLigne() - 1)) // Reste de la partie
				{
					return true;
				}
			}
		}
		return false;
	}

}
