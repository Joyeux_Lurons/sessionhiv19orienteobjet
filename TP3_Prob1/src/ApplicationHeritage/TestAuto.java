package ApplicationHeritage;

public class TestAuto {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Auto auto[] = new Auto[4];
		
		auto[0] = new Ford("Red",6999,2017,"Ford","Escape");
		auto[1] = new Chrysler("Blue",3200, 2016,"Chrysler","Voyager");
		auto[2] = new Mazda("Green",9990, 2016, "Mazda", "Protege");
		auto[3] = new Mercedes("Yellow", 10000, 2017, "Mercedes", "190");

		for (int i = 0; i < auto.length; i++) {
			auto[i].afficher();
		}
		
	}

}
