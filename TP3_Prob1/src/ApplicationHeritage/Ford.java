package ApplicationHeritage;

public class Ford extends Auto
{
	private int anFabrication;
	private String marque, modele;
	int NouvPrix;
	//Constructeur 1
	public Ford ()
	{
		super();
		if (this.anFabrication == 2017)
		{
			int prix = super.getPrix();
			this.NouvPrix = prix + 2000;
		}
		if (this.anFabrication == 2016)
		{
			int prix = super.getPrix();
			this.NouvPrix = prix - 200;
		}
	}
	//Constructeur 2. Faire appel au constructeur de la super classe
	public Ford (String couleur, int prix , int anFabrication, String marque, String modele)
	{
		super(couleur, prix);
		this.anFabrication = anFabrication;
		this.marque = marque;
		this.modele = modele;
		if (this.anFabrication == 2017)
		{
			int prix1 = super.getPrix();
			this.NouvPrix = prix1 + 2000;
		}
		if (this.anFabrication == 2016)
		{
			int prix1 = super.getPrix();
			this.NouvPrix = prix1 - 200;
		}
	}
	//red�finition de la m�thode estDisponible() de la super classe
	public void estDisponible()
	{
		super.estDisponible(); // Utilisation de la m�thode estDisponible de la super classe
		if (super.isEnStock() && super.isPreparee())
		{
			System.out.println("Votre Ford est disponible et pr�te!");
		}
	}
	public void afficher()  //red�finition de la m�thode afficher() de la super classe
	{
		super.afficher();
		System.out.println("Marque: " + this.marque);
		System.out.println("Modele: " + this.modele);
		System.out.println("Fabriqu�e en " + this.anFabrication);
		System.out.println("Prix: " + this.NouvPrix);
	}
	int getAnFabr()
	{
		return anFabrication;
	}
}
