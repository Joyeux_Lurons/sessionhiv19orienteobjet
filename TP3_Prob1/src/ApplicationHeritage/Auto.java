package ApplicationHeritage;
import java.io.*;

class Auto {
	private String couleur;
	private int prix;
	private boolean enStock, preparee;
	
	
	//Constructeur 1
	public Auto ()
	{
		this.enStock = true;
		this.preparee = true;
		this.couleur = "gris";
		this.prix = 4000;
	}

	/* Constructeur 2 permettant d'initialiser le prix et 
	 * la couleur d'un objet Auto
	 */
	public Auto (String couleur, int prix)
	{
		this();
		this.couleur = couleur;
		this.prix = prix;
	}
	
	
	public void estDisponible()
	{
		if(!this.enStock)
		{
			System.out.println("Voiture � commander!");
		}
		else if (this.enStock && this.preparee)
		{
			System.out.println("La voiture est pr�te!");
			//Messages � afficher si la voiture est en stock et pr�par�e
		}
		else if (!this.preparee)
		{
			System.out.println("Voiture en stock mais pas pr�par�e");
		}

	}
	public void afficher()
	{
		System.out.print("La voiture est de couleur " + this.couleur + " ");
		System.out.println("Se d�taille au prix de " + this.prix + " $");
		System.out.println("En stock? " + this.enStock);
		System.out.println("Pr�par�e? " + this.preparee);
	}
	
	public int getPrix() {
		return prix;
	}


	public void setPrix(int prix) {
		this.prix = prix;
	}

	//Autres m�thodes

	public String getCouleur() {
		return couleur;
	}


	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}
	
	public boolean isEnStock() {
		return enStock;
	}

	public void setEnStock(boolean enStock) {
		this.enStock = enStock;
	}

	public boolean isPreparee() {
		return preparee;
	}

	public void setPreparee(boolean preparee) {
		this.preparee = preparee;
	}
	
}

